/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.android.calendar;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.calendar.event.EditEventActivity;
import com.android.calendar.event.ExtendedProperty;
import com.android.calendar.icalendar.Attendee;
import com.android.calendar.icalendar.IcalendarUtils;
import com.android.calendar.icalendar.VEvent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

import ws.xsoh.etar.R;

public class EventUtils {
    private final static String TAG = EventUtils.class.getName();
    private EventUtils() {
    }

    @NonNull
    public static Bundle createBundleFromEvent(VEvent event, Context context) {
        Bundle bundle = new Bundle();

        bundle.putString(CalendarContract.Events.TITLE,
                IcalendarUtils.uncleanseString(event.getProperty(VEvent.SUMMARY)));
        bundle.putString(CalendarContract.Events.EVENT_LOCATION,
                IcalendarUtils.uncleanseString(event.getProperty(VEvent.LOCATION)));
        bundle.putString(CalendarContract.Events.DESCRIPTION,
                IcalendarUtils.uncleanseString(event.getProperty(VEvent.DESCRIPTION)));
        bundle.putString(ExtendedProperty.URL,
                IcalendarUtils.uncleanseString(event.getProperty(VEvent.URL)));
        bundle.putString(CalendarContract.Events.ORGANIZER,
                IcalendarUtils.uncleanseString(event.getProperty(VEvent.ORGANIZER)));
        bundle.putString(CalendarContract.Events.RRULE,
                IcalendarUtils.uncleanseString(event.getProperty(VEvent.RRULE)));

        if (event.mAttendees.size() > 0) {
            StringBuilder builder = new StringBuilder();
            for (Attendee attendee : event.mAttendees) {
                builder.append(attendee.mEmail);
                builder.append(",");
            }
            bundle.putString(Intent.EXTRA_EMAIL, builder.toString());
        }

        String dtStart = event.getProperty(VEvent.DTSTART);
        String dtStartParam = event.getPropertyParameters(VEvent.DTSTART);
        if (!TextUtils.isEmpty(dtStart)) {
            bundle.putLong(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                    getLocalTimeFromString(dtStart, dtStartParam, context));
        }

        String dtEnd = event.getProperty(VEvent.DTEND);
        String dtEndParam = event.getPropertyParameters(VEvent.DTEND);
        if (dtEnd != null && !TextUtils.isEmpty(dtEnd)) {
            bundle.putLong(CalendarContract.EXTRA_EVENT_END_TIME,
                    getLocalTimeFromString(dtEnd, dtEndParam, context));
        } else {
            // Treat start date as end date if un-specified
            dtEnd = dtStart;
            dtEndParam = dtStartParam;
        }

        boolean isAllDay = getLocalTimeFromString(dtEnd, dtEndParam, context)
                - getLocalTimeFromString(dtStart, dtStartParam, context) == 24*60*60*1000;


        if (isTimeStartOfDay(dtStart, dtStartParam, context)) {
            bundle.putBoolean(CalendarContract.EXTRA_EVENT_ALL_DAY, isAllDay);
        }
        //Check if some special property which say it is a "All-Day" event.

        String microsoft_all_day_event = event.getProperty("X-MICROSOFT-CDO-ALLDAYEVENT");
        if (!TextUtils.isEmpty(microsoft_all_day_event) && microsoft_all_day_event.equals("TRUE")) {
            bundle.putBoolean(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
        }

        bundle.putBoolean(EditEventActivity.EXTRA_READ_ONLY, true);

        return bundle;
    }

    private static boolean isTimeStartOfDay(String dtStart, String dtStartParam, Context context) {
        // convert to epoch milli seconds
        long timeStamp = getLocalTimeFromString(dtStart, dtStartParam, context);
        Date date = new Date(timeStamp);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String dateStr = dateFormat.format(date);
        if (dateStr.equals("00:00")) {
            return true;
        }
        return false;
    }

    private static long getLocalTimeFromString(String iCalDate, String iCalDateParam, Context context) {
        // see https://tools.ietf.org/html/rfc5545#section-3.3.5

        // FORM #2: DATE WITH UTC TIME, e.g. 19980119T070000Z
        if (iCalDate.endsWith("Z")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            try {
                format.parse(iCalDate);
                format.setTimeZone(TimeZone.getDefault());
                return format.getCalendar().getTimeInMillis();
            } catch (ParseException exception) {
                Log.e(TAG, "Can't parse iCalDate:", exception);
            }
        }

        // FORM #3: DATE WITH LOCAL TIME AND TIME ZONE REFERENCE, e.g. TZID=America/New_York:19980119T020000
        else if (iCalDateParam != null && iCalDateParam.startsWith("TZID=")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
            String timeZone = iCalDateParam.substring(5).replace("\"", "");
            // This is a pretty hacky workaround to prevent exact parsing of VTimezones.
            // It assumes the TZID to be refered to with one of the names recognizable by Java.
            // (which are quite a lot, see e.g. http://tutorials.jenkov.com/java-date-time/java-util-timezone.html)
            if (Arrays.asList(TimeZone.getAvailableIDs()).contains(timeZone)) {
                format.setTimeZone(TimeZone.getTimeZone(timeZone));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    String convertedTimeZoneId = android.icu.util.TimeZone
                            .getIDForWindowsID(timeZone, "001");
                    if (convertedTimeZoneId != null && !convertedTimeZoneId.equals("")) {
                        format.setTimeZone(TimeZone.getTimeZone(convertedTimeZoneId));
                    } else {
                        format.setTimeZone(TimeZone.getDefault());
                        Toast.makeText(
                                context,
                                context.getString(R.string.cal_import_error_time_zone_msg, timeZone),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    format.setTimeZone(TimeZone.getDefault());
                    Toast.makeText(
                            context,
                            context.getString(R.string.cal_import_error_time_zone_msg, timeZone),
                            Toast.LENGTH_SHORT).show();
                }
            }
            try {
                format.parse(iCalDate);
                return format.getCalendar().getTimeInMillis();
            } catch (ParseException exception) {
                Log.e(TAG, "Can't parse iCalDate:", exception);
            }
        }

        // ONLY DATE, e.g. 20190415
        else if (iCalDateParam != null && iCalDateParam.equals("VALUE=DATE")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            format.setTimeZone(TimeZone.getDefault());

            try {
                format.parse(iCalDate);
                return format.getCalendar().getTimeInMillis();
            } catch (ParseException exception) {
                Log.e(TAG, "Can't parse iCalDate:", exception);
            }
        }

        // FORM #1: DATE WITH LOCAL TIME, e.g. 19980118T230000
        else {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
            format.setTimeZone(TimeZone.getDefault());

            try {
                format.parse(iCalDate);
                return format.getCalendar().getTimeInMillis();
            } catch (ParseException exception) {
                Log.e(TAG, "Can't parse iCalDate:", exception);
            }
        }

        Toast.makeText(context, context.getString(R.string.cal_import_error_date_msg, iCalDate), Toast.LENGTH_SHORT).show();

        return System.currentTimeMillis();
    }
}
