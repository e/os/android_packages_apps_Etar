/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.android.calendar.event;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.MatrixCursor;

import com.android.calendar.Utils;

import java.lang.ref.WeakReference;

public class CalendarPickerQueryHandler extends AsyncQueryHandler {
    private final WeakReference<Activity> activityReference;
    private final WeakReference<CalendarPickerQueryListener> listenerReference;

    public CalendarPickerQueryHandler(ContentResolver contentResolver, Activity activity, CalendarPickerQueryListener calendarPickerQueryListener) {
        super(contentResolver);
        activityReference = new WeakReference<>(activity);
        listenerReference = new WeakReference<>(calendarPickerQueryListener);
    }

    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        super.onQueryComplete(token, cookie, cursor);

        final CalendarPickerQueryListener queryListener = listenerReference.get();

        if (queryListener == null) {
            return;
        }

        if (cursor == null || cursor.getCount() == 0) {
            queryListener.onCursorUnavailable();
            return;
        }

        final Activity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) {
            cursor.close();
            queryListener.onCalendarPickerError();
            return;
        }

        try (cursor) {
            MatrixCursor matrixCursor = Utils.matrixCursorFromCursor(cursor);
            queryListener.onCursorAvailable(matrixCursor);
        }
    }
}
