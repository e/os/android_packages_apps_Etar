/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.android.calendar.event;

import android.content.Context;
import android.database.Cursor;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.android.calendar.Utils;

import ws.xsoh.etar.R;

public class CalendarPickerAdapter extends ResourceCursorAdapter {

    public CalendarPickerAdapter(Context context, int resourceId, Cursor cursor) {
        super(context, resourceId, cursor, 0); // No selection flags added to avoid deprecated call
        setDropDownViewResource(R.layout.calendars_dropdown_item);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        View colorBar = view.findViewById(R.id.color);
        int colorColumn = cursor.getColumnIndexOrThrow(CalendarContract.Calendars.CALENDAR_COLOR);
        if (colorBar != null) {
            colorBar.setBackgroundColor(
                Utils.getDisplayColorFromColor(
                    context,
                    cursor.getInt(colorColumn)
                )
            );
        }

        int nameColumn = cursor.getColumnIndexOrThrow(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME);
        int ownerColumn = cursor.getColumnIndexOrThrow(CalendarContract.Calendars.OWNER_ACCOUNT);

        TextView name = view.findViewById(R.id.calendar_name);
        if (name == null) return;
        String displayName = cursor.getString(nameColumn);
        name.setText(displayName);

        TextView accountName = view.findViewById(R.id.account_name);
        if (accountName == null) return;
        accountName.setText(cursor.getString(ownerColumn));
        accountName.setVisibility(TextView.VISIBLE);
    }
}
