/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.android.calendar.event;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import ws.xsoh.etar.R;

/**
 * Allows the user to quickly import a multi event cal file.
 */
public class CalendarPickerDialogFragment extends DialogFragment implements AdapterView.OnItemSelectedListener, CalendarPickerQueryListener {

    public static final String FRAGMENT_TAG = "PICK_CALENDAR_DIALOG";
    public static final int SPINNER_DEFAULT_POSITION = 0;
    private static final int TOKEN_CALENDARS = 8;
    private static final int EVENT_INDEX_OWNER_ACCOUNT = 2;
    private static final int EVENT_INDEX_ACCOUNT_NAME = 11;
    private static final String BUNDLE_KEY_RESTORE_SELECTED_SPINNER_ITEM_POSITION = "selected_spinner_item_position";
    private static final String BUNDLE_KEY_RESTORE_NUMBER_OF_EVENTS = "number_of_events";
    private String mSyncAccountName = null;
    private long mPickedCalendarId = -1;
    private Cursor mCalendarsCursor;
    private String mOwnerAccount;
    private Spinner mCalendarsSpinner;
    private int mSelectedSpinnerItemPosition = SPINNER_DEFAULT_POSITION;
    // Instance of class that request to show this dialog and which expect the selected calendar
    private CalendarPickerDialogListener calendarPickerDialogListener;
    private int mNumberOfEvents;

    public CalendarPickerDialogFragment() {
    }

    public CalendarPickerDialogFragment(int numberOfEvents) {
        this.mNumberOfEvents = numberOfEvents;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof CalendarPickerDialogListener) {
            calendarPickerDialogListener = (CalendarPickerDialogListener) context;
        } else {
            throw new RuntimeException(context + " must implement CalendarPickerDialogListener");
        }
    }

    @Override
    public void onDetach() {
        calendarPickerDialogListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initiateCalendarQueryHandler();

        // Save and restore flow
        if (savedInstanceState != null) {
            mSelectedSpinnerItemPosition = savedInstanceState.getInt(BUNDLE_KEY_RESTORE_SELECTED_SPINNER_ITEM_POSITION);
            mNumberOfEvents = savedInstanceState.getInt(BUNDLE_KEY_RESTORE_NUMBER_OF_EVENTS);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BUNDLE_KEY_RESTORE_SELECTED_SPINNER_ITEM_POSITION, mSelectedSpinnerItemPosition);
        outState.putInt(BUNDLE_KEY_RESTORE_NUMBER_OF_EVENTS, mNumberOfEvents);
    }

    @Override
    public void onDestroyView() {
        closeCursor();
        super.onDestroyView();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // onCreateDialog() gets called before onCreateView(), so the view needs to be created and supplied here
        // getView() returns null at this point
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.pick_calendar_dialog, null);

        mCalendarsSpinner = view.findViewById(R.id.calendars_spinner);
        mCalendarsSpinner.setSelection(mSelectedSpinnerItemPosition);

        TextView dialogMessage = view.findViewById(R.id.pick_calendar_dialog_text);
        dialogMessage.setText(getString(R.string.pick_calendar_dialog_text, mNumberOfEvents));

        return new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.pick_calendar_dialog_title)
                .setView(view)
                .setPositiveButton(R.string.pick_calendar_dialog_validate,
                        (dialog, which) -> {
                            calendarPickerDialogListener.onCalendarPicked(mPickedCalendarId, mOwnerAccount, mSyncAccountName);
                            dismiss();
                        })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dismiss())
                .create();
    }

    private void closeCursor() {
        if (mCalendarsCursor != null && !mCalendarsCursor.isClosed()) {
            mCalendarsCursor.close();
        }
    }

    private void initiateCalendarQueryHandler() {
        final ContentResolver contentResolver = requireActivity().getContentResolver();
        if (contentResolver == null) {
            return;
        }

        CalendarPickerQueryHandler calendarPickerQueryHandler = new CalendarPickerQueryHandler(contentResolver, requireActivity(), this);

        calendarPickerQueryHandler.startQuery(
            TOKEN_CALENDARS,
            null,
            CalendarContract.Calendars.CONTENT_URI,
            EditEventHelper.CALENDARS_PROJECTION,
            EditEventHelper.CALENDARS_WHERE_WRITEABLE_VISIBLE,
            null /* selection args */,
            null /* sort order */
        );
    }

    public void setCalendarsCursor(Cursor cursor) {
        mCalendarsCursor = cursor;

        if (mCalendarsCursor.moveToFirst()) {
            mOwnerAccount = mCalendarsCursor.getString(EVENT_INDEX_OWNER_ACCOUNT);
            mSyncAccountName = mCalendarsCursor.getString(EVENT_INDEX_ACCOUNT_NAME);
            bindCalendarSpinnerUi();
        }
    }

    private void bindCalendarSpinnerUi() {
        CalendarPickerAdapter adapter = new CalendarPickerAdapter(requireContext(),
                R.layout.calendars_spinner_item,
                mCalendarsCursor);

        mCalendarsSpinner.setAdapter(adapter);
        mCalendarsSpinner.setOnItemSelectedListener(this);

        // Restore Spinner's position if there's any saved on configuration changes
        if (mSelectedSpinnerItemPosition != SPINNER_DEFAULT_POSITION) {
            mCalendarsSpinner.setSelection(mSelectedSpinnerItemPosition);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mSelectedSpinnerItemPosition = position;
        mPickedCalendarId = id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        mPickedCalendarId = -1;
    }

    @Override
    public void onCursorAvailable(Cursor cursor) {
        setCalendarsCursor(cursor);
    }

    @Override
    public void onCursorUnavailable() {
        dismiss();
        calendarPickerDialogListener.onCalendarUnavailable();
    }

    @Override
    public void onCalendarPickerError() {
        calendarPickerDialogListener.onCalendarPickerError();
    }

    public interface CalendarPickerDialogListener {
        void onCalendarPicked(long selectedCalendarId, String ownerAccount, String syncAccountName);

        void onCalendarUnavailable();

        void onCalendarPickerError();
    }
}

